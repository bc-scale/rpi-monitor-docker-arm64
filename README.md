# RPi-Monitor Docker ARM64

A way to build RPI-Monitor for Docker on ARM64

Only need to add execution bit on build.sh

```
sudo chmod +x build.sh
```

Then just build it :)

```
sudo bash ./build.sh
```
