#!/bin/bash
# ----------------------------------
# https://github.com/michaelmiklis/docker-rpi-monitor
# https://www.balena.io/docs/reference/base-images/base-images-ref/
# ----------------------------------

clear
cd "$(dirname "$0")" || exit 1

IMAGE_BASE=zogg/rpi-monitor
IMAGE_NAME_LATEST=${IMAGE_BASE}:latest

docker build \
    --build-arg TZ=Europe/Paris \
    --build-arg CONCURRENCY=$(nproc) \
    -t "${IMAGE_NAME_LATEST}" \
    . 2>&1 | tee build.log

exit 0
